
# 代号小天 (Project CAU_SKY)

## 中文版

China Agricultural University SKY ACGN Association Unity Development Group(under construction)

### 源文件参考自Project Chicago
## 初衷

在游戏历史上，曾经有一些由大学生自己凭借兴趣开发的游戏，在自己的大学内，甚至是对游戏史，影响深远。比较有代表性的是 "[北大侠客行](http://hk.pkuxkx.com/)"，由北京大学学生开发的一款文字冒险游戏，是国内mud游戏的典范，已经持续免费运营了20多年，至今还有很高的玩家在线率。游戏反映了学生的生活和乐趣，给自己和学校都留下了沉淀和记忆，是非常宝贵的遗产。

中国农业大学校内生活丰富, 小天作为十佳社团之一,是很有特色的学校和社团, 是得天独厚的游戏素材。作为ACG爱好者的同志，如果说这辈子连个像模像样的游戏都没做过，可能也算是白学了。要是能设计出一个游戏，在我们毕业后也能代代相传，持续运行下去，我们虽然谈不上特别伟大，但是会有一份特殊的荣誉感。

## 目标

设计一个以农大为背景的多人在线第一人称社区养成冒险游戏。初步想法是结合当年"第九城市"的社区和口袋妖怪等GBA游戏模式，引入你农特色元素，将整个农大和社团生活游戏化。可以想象成是"第九城市"社区与低像素游戏的结合（注：第九城市社区已经停运）。


## 游戏特色

游戏模式可能采用多人在线ARPG模式。

目前希望有的一些特色：

人物养成：
读书
种植
养殖
钓鱼
采矿

物品道具收集
装备
道具

战斗：
与黑帮战斗
社团战斗
体育竞技战斗

任务系统

社团公会

成就系统

等等

## 参考游戏 

### 口袋妖怪 火红/叶绿 (Pokémon FireRed/LeafGreen)

![Scheme](imgs/pokemon.png)
### 星露谷物语 (Stardew Valley)

![Scheme](imgs/stardew_valley.jpg)

### 塞尔达传说太阳神战士之谜 （Zelda Mystery of Solarus DX）

![Scheme](imgs/zelda.png)
### 恶搞之家 (Family Guy: The Quest for Stuff)

![Scheme](imgs/family_guy.png)

## 技术方法

鉴于我们是从零开始的初学者，也没有美工，技术上肯定也很落后，我们制作尽量从简，基于现有的教程和简单游戏进行改进。

本来初衷是设计网页游戏，但是做网页游戏需要会HTML5, CSS, Java/JavaScript，而且网页游戏的引擎貌似都不是很好。

后来接触了解了Unity游戏引擎，这个游戏引擎很屌，界面感觉做起来也相对人性化，而且支持拓展到页游，手游，端游，编程貌似也只要会C#一门就可以了。所以游戏引擎选用Unity。


## 游戏素材

因为我们时间精力有限，也没有专业美工，所以一切素材都用现成的。

### GBA游戏（口袋妖怪游戏）素材提取

[Advance Map](https://hackromtools.altervista.org/advance-map/)

### Solarus 素材提取

[Solarus](http://www.solarus-games.org/)是一个2D单机游戏引擎，上面的塞尔达传说太阳神战士之谜就是用这个引擎制作的。这个引擎应该能提取游戏素材，缺点是这个引擎无法制作多人在线游戏，所以我们不使用这个引擎来制作我们的游戏。

### 其他素材

还有些素材信息应该能从下面的“学习资料”教程里得到相关信息

## 学习资料

以下是我找的学习资料主题，你也可以根据主题相应google更多的资料

### Unity3D + C# 入门教程

[Udemy 课程](https://www.udemy.com/unitycourse2/learn/v4/overview)
[Udemy 课程 GitHub Repo](https://github.com/CompleteUnityDeveloper2)

### 游戏素材提取教程

[Advance Map 教程](https://hackromtools.altervista.org/advance-map/)

### 使用Unity制作口袋妖怪

[BrainStormGames 教程](https://www.youtube.com/user/BrainStormGames13)

[JesseEtzler 教程](https://www.youtube.com/playlist?list=PLxLNqnnCshwmIv7OsOZ08dZEVvPdbrrnz)

### Coding-Free Unity3D 2D游戏制作

[2D Games for Non-Programmers](https://unity3d.com/learn/tutorials/topics/2d-game-creation/2d-games-non-programmers?utm_campaign=engine_global_nurture_Nurture%20beginners%20PE&utm_content=02-nurture-beginner-tutorial&utm_medium=email&utm_source=Eloqua)

## 项目预期

待定